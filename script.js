// Функция для переключения темы
function toggleTheme() {
    const body = document.body;
    body.classList.toggle('theme1');
    body.classList.toggle('theme2');

    // Обновляем цвет текста в футере
    const footer = document.querySelector('.footer');
    const currentTheme = body.classList.contains('theme1') ? 'theme1' : 'theme2';

    if (footer) {
        footer.style.color = currentTheme === 'theme1' ? 'black' : 'gold';
    }

    // Обновляем цвет текста в навигационном меню
    const navigationItems = document.querySelectorAll('.navigation-list__items');
    if (navigationItems) {
        const navigationColor = currentTheme === 'theme1' ? 'white' : 'gold';
        navigationItems.forEach(item => {
            item.style.color = navigationColor;
        });
    }

    // Сохраняем выбранную тему в локальном хранилище
    localStorage.setItem('selectedTheme', currentTheme);
}

// Проверяем сохраненную тему в локальном хранилище
const savedTheme = localStorage.getItem('selectedTheme');
if (savedTheme) {
    document.body.classList.add(savedTheme);
}

// Обновляем цвет текста в футере и навигационном меню на основе сохраненной темы
const footer = document.querySelector('.footer');
if (footer && savedTheme) {
    footer.style.color = savedTheme === 'theme1' ? 'black' : 'gold';
}

const navigationItems = document.querySelectorAll('.navigation-list__items');
if (navigationItems && savedTheme) {
    const navigationColor = savedTheme === 'theme1' ? 'white' : 'gold';
    navigationItems.forEach(item => {
        item.style.color = navigationColor;
    });
}

// Добавляем обработчик события для кнопки смены темы
const themeToggleBtn = document.getElementById('theme-toggle');
if (themeToggleBtn) {
    themeToggleBtn.addEventListener('click', toggleTheme);
}
